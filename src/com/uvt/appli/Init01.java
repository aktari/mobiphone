package com.uvt.appli;

import com.uvt.service.*;
import java.util.Arrays;
import java.util.List;

import com.uvt.entities.*;

public class Init01 {

	private static List<String> lstOp = Arrays.asList(
			"TunisieTelecom",
			"Oooreedoo",
			"Orange");
	private static List<String> lstCarteTel = Arrays.asList(
			"Fixe",
			"Mobile");
	private static String OperateurListName = "Operateur";
	private static String CarteTelephoniqueListName = "CarteTelphonique";

	private static int stockCarteTelephonique = 20;

	public static void main(String[] args) {
		initConstants();
		initCollaborateurs();
		initCarteTelephoniques();
		initClients();
	}
	
	private static void initConstants() {
		mbh_listsImpl limpl = new mbh_listsImpl();
		mbh_valuesImpl vimpl = new mbh_valuesImpl();
		
		{
		mbh_lists li1 = new mbh_lists();
		li1.setName(OperateurListName);
		li1.setLibelle("operateur");
		li1.setDescription("nom de l operateur");
		limpl.create(li1);
		
		mbh_values va1;
		for (String val : lstOp) {
			va1 = new mbh_values();
			va1.setList(li1);
			va1.setValue(val);
			vimpl.create(va1);
		}
		}
		
		{
		mbh_lists li2 = new mbh_lists();
		
		li2.setName(CarteTelephoniqueListName);
		li2.setLibelle("CarteTelephonique");
		li2.setDescription("type de carte téléphonique");
		limpl.create(li2);
		mbh_values va2;
		for (String val : lstCarteTel) {
			va2 = new mbh_values();
			va2.setList(li2);
			va2.setValue(val);
			vimpl.create(va2);
		}
		}
		
		
		
		
	}
	
	private static void initCollaborateurs() {
		CollaborateurImpl impl = new CollaborateurImpl();
		
		{
		Collaborateur collaborateur = new Administrateur();
		collaborateur.setLogin("Admin");
		collaborateur.setPassword("pass123");
		collaborateur.setNumTelephone(123456789);
		collaborateur.setCourriel("admin@uvt.com");
		collaborateur.setPrenom("Mohamed");
		impl.create(collaborateur);
		}
		{
		Collaborateur collaborateur = new AgentCommercial();
		collaborateur.setLogin("AC01");
		collaborateur.setPassword("pass123");
		collaborateur.setNumTelephone(123456789);
		collaborateur.setCourriel("act01@uvt.com");
		collaborateur.setPrenom("Anis");
		impl.create(collaborateur);
		}
	}

	private static void initCarteTelephoniques() {
		CarteTelephoniqueImpl impl = new CarteTelephoniqueImpl();
		CarteTelephoniqueArticleImpl impl2 = new CarteTelephoniqueArticleImpl();
		List<Float> lstPrixDeVente = Arrays.asList(10.0f, 20.0f, 5.0f);
		CarteTelephonique carteTelephonique;

		mbh_valuesImpl vi = new mbh_valuesImpl();
		List<mbh_values> lvOp = vi.findAllmbh_values_byListName(OperateurListName);
		List<mbh_values> lvCT = vi.findAllmbh_values_byListName(CarteTelephoniqueListName);
		
		for (mbh_values opr : lvOp) {
			for (mbh_values CTe : lvCT) {
				for(Float prixDeVente : lstPrixDeVente) {
					carteTelephonique = new CarteTelephonique();
					carteTelephonique.setDureeDeValiditeEnJours(120);
					carteTelephonique.setOperateur(opr);
					carteTelephonique.setPrixDeVente(prixDeVente);
					carteTelephonique.setSeuilStock(stockCarteTelephonique);
					carteTelephonique.setTypeCarteTelephonique(CTe);
					impl.create(carteTelephonique);
					
					for (int i =0;i<stockCarteTelephonique; i++) {
						CarteTelephoniqueArticle carteTelephoniqueArticle = new CarteTelephoniqueArticle();
						carteTelephoniqueArticle.setProduit(carteTelephonique);
						impl2.create(carteTelephoniqueArticle);
					}
				}
			}
			
		}
		
		
	}

	private static void initClients() {
		ClientMPImpl impl = new ClientMPImpl();
		
		ClientMP c = new ClientMP();
		c.setAdresse("BenArous");
		c.setCourriel("a.b@uvt.com");
		c.setLogin("login123456");
		c.setNom("client456789");
		c.setNumTelephone(95521652);
		c.setPrenom("Nejd");
		impl.create(c);		
		
	}
	
}
