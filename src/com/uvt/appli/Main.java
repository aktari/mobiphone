package com.uvt.appli;
import java.util.*;
import com.uvt.service.*;
import com.uvt.entities.*;

public class Main {

	static Scanner scanner = new Scanner(System.in);
	private static String OperateurListName = "Operateur";
	
    public static void main(String[] args) {
    	Collaborateur collaborateurEncours = null;
    	
    	while (collaborateurEncours == null) {
    	System.out.print("Enter your login:");
    	String username = scanner.nextLine();  
    	System.out.println();
    	
    	System.out.print("Enter your password:");
    	String password = scanner.nextLine();
    	System.out.println();
    	
    	CollaborateurImpl collaborateurImpl = new CollaborateurImpl();
    	List<Collaborateur> lstCollaborateur = collaborateurImpl.findAllCollaborateur();
    	//System.out.println("nb collaborateur = "+lstCollaborateur.size());
    	for(Collaborateur collaborateur : lstCollaborateur) {
    		System.out.println(collaborateur.getLogin()+" "+collaborateur.getPassword());
    		if (collaborateur.getLogin().equalsIgnoreCase(username)&&
    			collaborateur.getPassword().equals(password)) {
    			collaborateurEncours = collaborateur;
    			break;
    		}
    	}
    	if (collaborateurEncours == null) System.out.println("collaborateur "+username+" non trouve ");
    	}
    	
    	if (collaborateurEncours instanceof Administrateur) {
    		menuAdministrateur(collaborateurEncours);
    	} else if (collaborateurEncours instanceof AgentCommercial) {
    		menuAgentCommercial(collaborateurEncours);
    	}
    }

    private static void menuAdministrateur(Collaborateur collaborateurEncours) {
		if (!(collaborateurEncours instanceof Administrateur)) {
			System.err.println("Le collaborateur n est pas un admin");
		}
		System.out.println("Bonjour "+collaborateurEncours.getPrenom());
		int choix = 0;
		
		while (choix<1 || choix>2) {
			System.out.println("1. rechercher une personne existente et la modifier ");
			System.out.println("2. entrer un nouveau collaborateur ");
			System.out.print("Veuillez choisir l'un des cas :");
			choix = scanner.nextInt();
			System.out.println();
		}
		if (choix == 1) {
			System.out.println("Ce cas est en cours de developpement, merci de revenir vers votre administrateur !");
			return;
		} else if (choix == 2) {
			System.out.println("Quel profil voulez-vous donner au nouveau collaborateur ?");
			int ch2 = 0;
			while (ch2<1 || ch2>3) {
				System.out.println("1. AgentCommercial");
				System.out.println("2. Comptable");
				System.out.println("3. ResponsableCommercial");
				ch2 = scanner.nextInt();
			}
			Collaborateur collaborateur;
			if (ch2 == 1) collaborateur = new AgentCommercial();
			else if (ch2 == 2) collaborateur = new Comptable();
			else collaborateur = new AgentCommercial();
			
			{System.out.print("Entrer le nom: ");
			String nom = scanner.nextLine();
			System.out.println();
			collaborateur.setNom(nom);}
			{System.out.print("Entrer le prenom : ");
			String prenom = scanner.nextLine();
			System.out.println();
			collaborateur.setPrenom(prenom);}
			{System.out.print("Entrer l adresse : ");
			String adresse = scanner.nextLine();
			System.out.println();
			collaborateur.setAdresse(adresse);}
			
			CollaborateurImpl impl = new CollaborateurImpl();
			{
				String courriel = "";
				while (courriel.isEmpty()) {
					System.out.print("Entrer le courriel du client: ");
					courriel = scanner.nextLine();
					String mail = courriel;
					System.out.println();
					{
						Collaborateur c = impl.findAllCollaborateur().stream()  // Convert to steam
				                .filter(x -> mail.equals(x.getCourriel()))        // we want "jack" only
				                .findAny()                                      // If 'findAny' then return found
				                .orElse(null);
						if (c != null)  {
							System.out.println("Le courriel est deja trouve, voulez-vous revenir au menu principal (O/N) ?");
							String response = scanner.nextLine();
							if ("O".equalsIgnoreCase(response)) {
								menuAdministrateur(collaborateurEncours);
								return;
							}
							courriel = "";
						}
					}
				}
				collaborateur.setCourriel(courriel);
			}
			{
				Integer numTelephone = 0;
				while (numTelephone == 0) {
				System.out.print("Entrer le numero de telephone du client: ");
				String snumTel = scanner.nextLine();
				try {
					numTelephone = Integer.parseInt(snumTel);
				} catch(java.lang.NumberFormatException ex) {
					System.out.println("veuillez entrer le t�l�phone sous forme d'entier");
				}
				Integer num = numTelephone;
				System.out.println();
				
				if (num != 0) {
				Collaborateur c = impl.findAllCollaborateur().stream()  // Convert to steam
		                .filter(x -> num ==x.getNumTelephone() )        // we want "jack" only
		                .findAny()                                      // If 'findAny' then return found
		                .orElse(null);
				if (c != null) {
					System.out.println("Le numTelephone est deja trouve, voulez-vous revenir au menu principal (O/N) ?");
					String response = scanner.nextLine();
					if ("O".equalsIgnoreCase(response)) {
						menuAdministrateur(collaborateurEncours);
						return;
					}
					numTelephone = 0;
				}
				}}
				collaborateur.setNumTelephone(numTelephone);
			}
			impl.create(collaborateur);
		}
    }
    
    private static void menuAgentCommercial(Collaborateur collaborateurEncours) {
		if (!(collaborateurEncours instanceof AgentCommercial)) {
			System.err.println("Le collaborateur n est pas un agent commercial");
		}
		System.out.println("Bonjour "+collaborateurEncours.getPrenom());
		ClientMP client = null; 
		while (client == null) {
			client = choixClient();
			if (client != null)
				System.out.println("le client choisi est : "+client.getNom());
			else {
				System.out.println("le client n'a pas ete trouve --> revenir au menu de choix client ");
			}
		}
		
		System.out.println("Merci de choisir des articles");
		List<Produit> lstProduit = new ArrayList<Produit>();
		String choixAjout = null;
		do {
			Integer qte = 0;
			do {
				System.out.println("choix quantite : ");
				qte = scanner.nextInt();
			} while (qte<1);
			List<Produit> lstProduit0 = null;
			do {
				lstProduit0 = choixArticle(qte);
			} while(lstProduit0==null||lstProduit0.isEmpty());
			lstProduit.addAll(lstProduit0);
			System.out.println("voulez vous ajouter des articles (O/N) :");
			choixAjout = scanner.nextLine();
		} while(!lstProduit.isEmpty() && !"O".equals(choixAjout));
    }
    
    private static ClientMP choixClient() {
    	ClientMP client = null;
		int choix1 = -1;
		while (choix1<1 || choix1>2) {
			System.out.println("1. choix d'un client ");
			System.out.println("2. entrer un nouveau client ");
			System.out.print("Veuillez choisir l'un des cas :");
			choix1 = scanner.nextInt();
			System.out.println();
		}
		if (choix1==1) {
			int choix2 = -1;
			
			while (client == null && (choix2<1 || choix2>2)) {
				System.out.println("1. recherche par courriel ");
				System.out.println("2. recherche par numTelephone ");
				System.out.print("Veuillez choisir l'un des cas :");
				choix2 = scanner.nextInt();
				System.out.println();
				if (choix2==1) {
					System.out.print("Veuillez entrer le courriel du client :");
					String courriel = scanner.nextLine();
					System.out.println();
					ClientMPImpl clientMPImpl = new ClientMPImpl();
					ClientMP clientMP = clientMPImpl.findAllClientMP().stream()  // Convert to steam
			                .filter(x -> courriel.equals(x.getCourriel()))        // we want "jack" only
			                .findAny()                                      // If 'findAny' then return found
			                .orElse(null);
					client = clientMP;
				} else if (choix2==2) {
					System.out.print("Veuillez entrer le numero de telephone du client :");
					Integer numTelephone = scanner.nextInt();
					System.out.println();
					ClientMPImpl clientMPImpl = new ClientMPImpl();
					ClientMP clientMP = clientMPImpl.findAllClientMP().stream()  // Convert to steam
			                .filter(x -> numTelephone ==x.getNumTelephone() )        // we want "jack" only
			                .findAny()                                      // If 'findAny' then return found
			                .orElse(null);
					client = clientMP;
				}
				if (client == null) {
					System.out.println("Le client n a pas ete trouve ");
				}
			}
		} else if (choix1==2) {
			ClientMP clientMP = new ClientMP();
			{System.out.println("Entrer le nom du client: ");
			String nom = scanner.nextLine();
			System.out.println();
			clientMP.setNom(nom);}
			{System.out.println("Entrer le prenom du client: ");
			String prenom = scanner.nextLine();
			System.out.println();
			clientMP.setPrenom(prenom);}
			{System.out.println("Entrer l adresse du client: ");
			String adresse = scanner.nextLine();
			System.out.println();
			clientMP.setAdresse(adresse);}
			ClientMPImpl clientMPImpl = new ClientMPImpl();
			{
				String courriel = "";
				while (courriel.isEmpty()) {
					System.out.print("Entrer le courriel du client: ");
					courriel = scanner.nextLine();
					String mail = courriel;
					System.out.println();
					{
						ClientMP clientMP1 = clientMPImpl.findAllClientMP().stream()  // Convert to steam
				                .filter(x -> mail.equals(x.getCourriel()))        // we want "jack" only
				                .findAny()                                      // If 'findAny' then return found
				                .orElse(null);
						if (clientMP1 != null)  {
							System.out.println("Le courriel est deja trouve, voulez-vous revenir au menu principal (O/N) ?");
							String response = scanner.nextLine();
							if ("O".equalsIgnoreCase(response)) {
								return choixClient();
							}
							courriel = "";
						}
					}
				}
				clientMP.setCourriel(courriel);
			}
			{
				Integer numTelephone = 0;
				while (numTelephone == 0) {
				System.out.print("Entrer le numero de telephone du client: ");
				numTelephone = scanner.nextInt();
				Integer num = numTelephone;
				System.out.println();
				
				{
				ClientMP clientMP1 = clientMPImpl.findAllClientMP().stream()  // Convert to steam
		                .filter(x -> num ==x.getNumTelephone() )        // we want "jack" only
		                .findAny()                                      // If 'findAny' then return found
		                .orElse(null);
				if (clientMP1 != null) {
					System.out.println("Le numTelephone est deja trouve, voulez-vous revenir au menu principal (O/N) ?");
					String response = scanner.nextLine();
					if ("O".equalsIgnoreCase(response)) {
						return choixClient();
					}
					numTelephone = 0;
				}
				}}
				clientMP.setNumTelephone(numTelephone);
			}
			clientMPImpl.create(clientMP);
			client = clientMP;
		}
		return client;
    }
 
    private static List<Produit> choixArticle(Integer qte) {
    	System.out.println("Voule-vous : ");
    	System.out.println("1. Une ligne telephonique");
    	System.out.println("2. Un telephone mobile");
    	System.out.println("3. Une Cle3G");
    	System.out.println("4. Une Carte Telephonique");
    	Integer choixProduit1 = 0;
    	do {
	    	System.out.println("faites votre choix : ");
	    	choixProduit1 = scanner.nextInt();
    	} while (choixProduit1 < 1 || choixProduit1>4);
    	if(choixProduit1 == 1) {
    		int op = 0;
    		mbh_valuesImpl vi = new mbh_valuesImpl();
    		List<mbh_values> lvOp = vi.findAllmbh_values_byListName(OperateurListName);
			String msg = "";int ind1 = 1;
			for(mbh_values val : lvOp) {msg += ""+ind1+++val.getValue()+" ";}
    		
			mbh_values opT;
    		while (op<1 || op>3) {
	    		System.out.println("choix  operateur : "+msg);
	    		op = scanner.nextInt();
    		}
    		opT = lvOp.get(op-1);
    		Float montantMinCom = 0F;
    		System.out.println("choix  montant min communication par mois : ");
    		montantMinCom = scanner.nextFloat();
    		LigneTelephoniqueImpl ligneTelephoniqueImpl = new LigneTelephoniqueImpl();
    		LigneTelephonique lTFound = null;
    		for (LigneTelephonique lt : ligneTelephoniqueImpl.findAllLigneTelephonique()) {
    			if (lt.getOperateur() == opT && lt.getMontantMinConsommationParMois() == montantMinCom) {
    				lTFound = lt;
    			}
    		}
    	}
    	return null;
    }

}
