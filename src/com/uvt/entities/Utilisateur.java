package com.uvt.entities;

import com.uvt.entities.Personne;
import java.io.Serializable;
import java.lang.String;
import javax.persistence.*;

/**
 * Entity implementation class for Entity: Utilisateur
 *
 */
@Entity
public class Utilisateur extends Personne implements Serializable {

	private String login;
	private String password;
	private static final long serialVersionUID = 1L;

	public Utilisateur() {
		super();
	}   

	public String getLogin() {
		return this.login;
	}

	public void setLogin(String login) {
		this.login = login;
	}   
	public String getPassword() {
		return this.password;
	}

	public void setPassword(String password) {
		this.password = password;
	}
   
}
