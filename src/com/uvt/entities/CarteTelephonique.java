package com.uvt.entities;

import com.uvt.entities.Produit;
import java.io.Serializable;
import java.lang.Integer;
import javax.persistence.*;

/**
 * Entity implementation class for Entity: CarteTelephonique
 *
 */
@Entity
public class CarteTelephonique extends Produit implements Serializable {
	
	private Integer dureeDeValiditeEnJours;
	private mbh_values operateur;
	private mbh_values typeCarteTelephonique;
	private static final long serialVersionUID = 1L;

	public CarteTelephonique() {
		super();
	}
	public Integer getDureeDeValiditeEnJours() {
		return this.dureeDeValiditeEnJours;
	}

	public void setDureeDeValiditeEnJours(Integer dureeDeValiditeEnJours) {
		this.dureeDeValiditeEnJours = dureeDeValiditeEnJours;
	}   
	public mbh_values getTypeCarteTelephonique() {
		return typeCarteTelephonique;
	}
	public void setTypeCarteTelephonique(mbh_values type) {
		this.typeCarteTelephonique = type;
	}
	public mbh_values getOperateur() {
		return this.operateur;
	}

	public void setOperateur(mbh_values operateur) {
		this.operateur = operateur;
	}
   
}
