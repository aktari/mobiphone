package com.uvt.entities;

import com.uvt.entities.mbh_lists;
import java.io.Serializable;
import java.lang.Integer;
import java.lang.String;
import javax.persistence.*;

/**
 * Entity implementation class for Entity: mbh_values
 *
 */
@Entity

public class mbh_values implements Serializable {

	@Id
	@GeneratedValue (strategy=GenerationType.IDENTITY) //pour autoincrement
	private Integer Id;
	@ManyToOne
	private mbh_lists list;
	private String value;
	private static final long serialVersionUID = 1L;

	public mbh_values() {
		super();
	}   
	public Integer getId() {
		return this.Id;
	}
	public mbh_lists getList() {
		return this.list;
	}

	public void setList(mbh_lists list) {
		this.list = list;
	}   
	public String getValue() {
		return this.value;
	}

	public void setValue(String value) {
		this.value = value;
	}
   
}
