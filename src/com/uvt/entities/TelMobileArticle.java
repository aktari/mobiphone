package com.uvt.entities;

import com.uvt.entities.Article;
import java.io.Serializable;
import javax.persistence.*;

/**
 * Entity implementation class for Entity: TelMobileArticle
 *
 */
@Entity
public class TelMobileArticle extends Article implements Serializable {
	
	private static final long serialVersionUID = 1L;

	public TelMobileArticle() {
		super();
	}
   
}
