package com.uvt.entities;

import java.io.Serializable;
import java.lang.Integer;
import javax.persistence.*;

/**
 * Entity implementation class for Entity: Article
 *
 */
@Entity
public class Article implements Serializable {

	@Id
	@GeneratedValue (strategy=GenerationType.IDENTITY)
	private Integer referenceArticle;
	@ManyToOne
	private Produit produit;
	private static final long serialVersionUID = 1L;

	public Article() {
		super();
	}
	public Integer getReferenceArticle() {
		return this.referenceArticle;
	}

	public Produit getProduit() {
		return produit;
	}
	public void setProduit(Produit produit) {
		this.produit = produit;
	}
   
}
