package com.uvt.entities;

import com.uvt.entities.Produit;
import java.io.Serializable;
import java.lang.Float;
import javax.persistence.*;

/**
 * Entity implementation class for Entity: Cle3G
 *
 */
@Entity
public class Cle3G extends Produit implements Serializable {

	private Float debitConnexion;
	private mbh_values operateur;
	private Float capaciteMaxOnDownload;
	private static final long serialVersionUID = 1L;

	public Cle3G() {
		super();
	}
	public Float getDebitConnexion() {
		return this.debitConnexion;
	}

	public void setDebitConnexion(Float debitConnexion) {
		this.debitConnexion = debitConnexion;
	}   
	public mbh_values getOperateur() {
		return this.operateur;
	}

	public void setOperateur(mbh_values operateur) {
		this.operateur = operateur;
	}   
	public Float getCapaciteMaxOnDownload() {
		return this.capaciteMaxOnDownload;
	}

	public void setCapaciteMaxOnDownload(Float capaciteMaxOnDownload) {
		this.capaciteMaxOnDownload = capaciteMaxOnDownload;
	}
   
}
