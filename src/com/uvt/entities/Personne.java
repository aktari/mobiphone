package com.uvt.entities;

import java.io.Serializable;
import java.lang.Integer;
import java.lang.String;
import javax.persistence.*;

/**
 * Entity implementation class for Entity: Personne
 *
 */
@Entity
public class Personne implements Serializable {

	@Id 
	@GeneratedValue (strategy=GenerationType.IDENTITY) //pour autoincrement
	private Integer reference;
	private String nom;
	private String prenom;
	private String courriel;
	private Integer numTelephone;
	private String adresse;
	private static final long serialVersionUID = 1L;

	public Personne() {
		super();
	}   
	public String getNom() {
		return this.nom;
	}

	public void setNom(String nom) {
		this.nom = nom;
	}   
	public String getPrenom() {
		return this.prenom;
	}

	public void setPrenom(String prenom) {
		this.prenom = prenom;
	}   
	
	public String getCourriel() {
		return this.courriel;
	}

	public void setCourriel(String courriel) {
		this.courriel = courriel;
	}   
	public Integer getNumTelephone() {
		return this.numTelephone;
	}

	public void setNumTelephone(Integer numTelephone) {
		this.numTelephone = numTelephone;
	}   
	public String getAdresse() {
		return this.adresse;
	}

	public void setAdresse(String adresse) {
		this.adresse = adresse;
	}
	public Integer getReference() {
		return reference;
	}

   
}
