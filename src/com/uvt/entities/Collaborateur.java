package com.uvt.entities;

import com.uvt.entities.Utilisateur;
import java.io.Serializable;
import javax.persistence.*;

/**
 * Entity implementation class for Entity: Collaborateur
 *
 */
@Entity
public class Collaborateur extends Utilisateur implements Serializable {

	
	private static final long serialVersionUID = 1L;

	public Collaborateur() {
		super();
	}
   
}
