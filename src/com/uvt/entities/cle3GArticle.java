package com.uvt.entities;

import com.uvt.entities.Article;
import java.io.Serializable;
import java.lang.String;
import javax.persistence.*;

/**
 * Entity implementation class for Entity: cle3GArticle
 *
 */
@Entity
public class cle3GArticle extends Article implements Serializable {
	
	private String numSerie;
	private static final long serialVersionUID = 1L;

	public cle3GArticle() {
		super();
	}   
	
	public String getNumSerie() {
		return this.numSerie;
	}

	public void setNumSerie(String numSerie) {
		this.numSerie = numSerie;
	}
   
}
