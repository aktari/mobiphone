package com.uvt.entities;

import com.uvt.entities.Article;
import java.io.Serializable;
import java.lang.Integer;
import javax.persistence.*;

/**
 * Entity implementation class for Entity: NumeroTelephonique
 *
 */
@Entity
public class NumeroTelephonique extends Article implements Serializable {

	
	private Integer NumeroTelephonique;
	private static final long serialVersionUID = 1L;

	public NumeroTelephonique() {
		super();
	}   
	public Integer getNumeroTelephonique() {
		return this.NumeroTelephonique;
	}
	public void setNumeroTelephonique(Integer NumeroTelephonique) {
		this.NumeroTelephonique = NumeroTelephonique;
	}
	
}
