package com.uvt.entities;

import com.uvt.entities.Produit;
import java.io.Serializable;
import java.lang.Float;
import javax.persistence.*;

/**
 * Entity implementation class for Entity: LigneTelephonique
 *
 */
@Entity
public class LigneTelephonique extends Produit implements Serializable {

	private Float montantMinConsommationParMois;
	private mbh_values operateur;
	
	public mbh_values getOperateur() {
		return operateur;
	}
	public void setOperateur(mbh_values operateur) {
		this.operateur = operateur;
	}

	private static final long serialVersionUID = 1L;

	public LigneTelephonique() {
		super();
	}   
	public Float getMontantMinConsommationParMois() {
		return this.montantMinConsommationParMois;
	}

	public void setMontantMinConsommationParMois(Float montantMinConsommationParMois) {
		this.montantMinConsommationParMois = montantMinConsommationParMois;
	}
	
   
}
