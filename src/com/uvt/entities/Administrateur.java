package com.uvt.entities;

import com.uvt.entities.Collaborateur;
import java.io.Serializable;
import javax.persistence.*;

/**
 * Entity implementation class for Entity: Administrateur
 *
 */
@Entity
public class Administrateur extends Collaborateur implements Serializable {

	
	private static final long serialVersionUID = 1L;

	public Administrateur() {
		super();
	}
   
}
