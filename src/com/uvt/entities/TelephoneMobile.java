package com.uvt.entities;

import com.uvt.entities.Produit;
import java.io.Serializable;
import java.lang.String;
import javax.persistence.*;

/**
 * Entity implementation class for Entity: TelephoneMobile
 *
 */
@Entity
public class TelephoneMobile extends Produit implements Serializable {
	
	private String Marque;
	private String Modele;
	private static final long serialVersionUID = 1L;

	public TelephoneMobile() {
		super();
	}   
	public String getMarque() {
		return this.Marque;
	}

	public void setMarque(String Marque) {
		this.Marque = Marque;
	}   
	public String getModele() {
		return this.Modele;
	}

	public void setModele(String Modele) {
		this.Modele = Modele;
	}
   
}
