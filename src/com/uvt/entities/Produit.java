package com.uvt.entities;

import java.io.Serializable;
import java.lang.Float;
import java.lang.Integer;
import javax.persistence.*;

/**
 * Entity implementation class for Entity: Produit
 *
 */
@Entity
public class Produit implements Serializable {

	@Id
	@GeneratedValue (strategy=GenerationType.IDENTITY)
	private Integer reference;
	private Float prixDeVente;
	private Integer seuilStock;
	private static final long serialVersionUID = 1L;

	public Produit() {
		super();
	}   
	public Integer getReference() {
		return this.reference;
	}
	public Float getPrixDeVente() {
		return this.prixDeVente;
	}

	public void setPrixDeVente(Float prixDeVente) {
		this.prixDeVente = prixDeVente;
	}
	
	public Integer getSeuilStock() {
		return seuilStock;
	}
	
	public void setSeuilStock(Integer seuilStock) {
		this.seuilStock = seuilStock;
	}
	
}
