package com.uvt.entities;

import java.io.Serializable;
import java.lang.Integer;
import java.lang.String;
import javax.persistence.*;

/**
 * Entity implementation class for Entity: mbh_lists
 *
 */
@Entity

public class mbh_lists implements Serializable {

	   
	@Id
	@GeneratedValue (strategy=GenerationType.IDENTITY) //pour autoincrement
	private Integer id;
	private String name;
	private String description;
	private String libelle;
	private static final long serialVersionUID = 1L;

	public mbh_lists() {
		super();
	}   
	public Integer getId() {
		return this.id;
	}
	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}   
	public String getDescription() {
		return this.description;
	}

	public void setDescription(String description) {
		this.description = description;
	}   
	public String getLibelle() {
		return this.libelle;
	}

	public void setLibelle(String libelle) {
		this.libelle = libelle;
	}
   
}
