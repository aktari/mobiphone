package com.uvt.entities;

import com.uvt.entities.Article;
import java.io.Serializable;
import java.lang.String;
import javax.persistence.*;

/**
 * Entity implementation class for Entity: CarteTelephoniqueArticle
 *
 */
@Entity
public class CarteTelephoniqueArticle extends Article implements Serializable {
	
	private String codeIdentification;
	private static final long serialVersionUID = 1L;

	public CarteTelephoniqueArticle() {
		super();
	}   

	public String getCodeIdentification() {
		return this.codeIdentification;
	}

	public void setCodeIdentification(String codeIdentification) {
		this.codeIdentification = codeIdentification;
	}
   
}
