package com.uvt.service;

import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import com.uvt.util.JPAutil;
import com.uvt.entities.TelephoneMobile;

public class TelephoneMobileImpl extends AbstractFacade<TelephoneMobile> implements TelephoneMobileInterface {

	//@PersistenceContext(unitName = "MobiPhone")
    //private EntityManager em;
	private EntityManager em = JPAutil.getEntityManager("MobiPhone");

    @Override
    protected EntityManager getEntityManager() {return em;}

//    public TelephoneMobileImpl() {
//    	super(TelephoneMobileImpl.class);
//    }

	@Override
	public List<TelephoneMobile> findAllTelephoneMobile() {
		try {
		    return em.createQuery("select r from TelephoneMobile r").getResultList();
		} catch (Exception e) {
		    System.out.println("findAllTelephoneMobile --> e.getMessage() = " + e.getMessage());
		    return null;
		}
	}

}
