package com.uvt.service;
import java.util.List;
import com.uvt.entities.Utilisateur;
public interface UtilisateurInterface {
void create(Utilisateur utilisateur);
void edit(Utilisateur utilisateur);
void remove(Utilisateur utilisateur);
List<Utilisateur> findAllUtilisateur();
}
