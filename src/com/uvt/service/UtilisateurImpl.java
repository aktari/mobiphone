package com.uvt.service;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import com.uvt.util.JPAutil;
import com.uvt.entities.Utilisateur;
public class UtilisateurImpl extends AbstractFacade<Utilisateur> implements UtilisateurInterface {
	private EntityManager em = JPAutil.getEntityManager("MobiPhone");
	@Override
	protected EntityManager getEntityManager() {return em;}
	@Override
	public List<Utilisateur> findAllUtilisateur() {
		try {
			return em.createQuery("select r from Utilisateur r").getResultList();
		} catch (Exception e) {
			System.out.println("findAllUtilisateur --> e.getMessage() = " + e.getMessage());
			return null;
		}
	}
}
