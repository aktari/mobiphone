package com.uvt.service;
import java.util.List;
import javax.persistence.EntityManager;
import com.uvt.util.JPAutil;
import com.uvt.entities.CarteTelephoniqueArticle;
public class CarteTelephoniqueArticleImpl extends AbstractFacade<CarteTelephoniqueArticle> implements CarteTelephoniqueArticleInterface {
	private EntityManager em = JPAutil.getEntityManager("MobiPhone");
	@Override
	protected EntityManager getEntityManager() {return em;}
	@SuppressWarnings("unchecked")
	@Override
	public List<CarteTelephoniqueArticle> findAllCarteTelephoniqueArticle() {
		try {
			return em.createQuery("select r from CarteTelephoniqueArticle r").getResultList();
		} catch (Exception e) {
			System.out.println("findAllCarteTelephoniqueArticle --> e.getMessage() = " + e.getMessage());
			return null;
		}
	}
}
