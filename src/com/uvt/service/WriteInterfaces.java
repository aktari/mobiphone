package com.uvt.service;

import java.util.*;
import java.nio.file.*;
import  java.nio.charset.Charset;
import static java.util.stream.Stream.of;
import static java.util.stream.Collectors.toCollection;

public class WriteInterfaces {

    public static void main(String[] args) {
    	Path path = FileSystems.getDefault().getPath(".").toAbsolutePath();
    	System.out.println(path.toString());
    	/*
    	ArrayList<String> lstEntite = of("LigneTelephonique",  
    		"CarteTelephonique", "Cle3G", "Client", "ClientMP", 
    		"Collaborateur", "Personne", "Produit", 
    		"Utilisateur")
    			.collect(toCollection(ArrayList::new));
    	
    	ArrayList<String> lstEntite = of("CarteTelephoniqueArticle", 
    			"cle3GArticle", "TelMobileArticle")
    			.collect(toCollection(ArrayList::new)); */
    	ArrayList<String> lstEntite = of("mbh_lists", "mbh_values")
    			.collect(toCollection(ArrayList::new));
    	List<String> lines = new ArrayList<String>();
    	for (String entite : lstEntite) {
	    	//String entite = "LigneTelephonique";
	    	String nomEntite = "";
	    	for (int i=0; i < entite.length(); i++) {
	    		nomEntite+=(i==0)?Character.toLowerCase(entite.charAt(i)):entite.charAt(i);
	    	}
	    	System.out.println(entite);
	    	lines.clear();
			lines.addAll(Arrays.asList("package com.uvt.service;", 
					"import java.util.List;",
					"import com.uvt.entities."+entite+";",
					"public interface "+entite+"Interface {",
					"void create("+entite+" "+nomEntite+");",
					"void edit("+entite+" "+nomEntite+");",
					"void remove("+entite+" "+nomEntite+");",
					"List<"+entite+"> findAll"+entite+"();",
					"}"
					));
			
			Path file = Paths.get(entite+"Interface.java");
			try {
				Files.write(file, lines, Charset.forName("UTF-8"));
				System.out.println("write done ...");
			} catch(java.io.IOException ex) {
				System.out.println("IOException error thrown");
			}
    	}
		//Files.write(file, lines, Charset.forName("UTF-8"), StandardOpenOption.APPEND);

    }

}
