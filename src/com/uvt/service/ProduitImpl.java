package com.uvt.service;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import com.uvt.util.JPAutil;
import com.uvt.entities.Produit;
public class ProduitImpl extends AbstractFacade<Produit> implements ProduitInterface {
	private EntityManager em = JPAutil.getEntityManager("MobiPhone");
	@Override
	protected EntityManager getEntityManager() {return em;}
	@Override
	public List<Produit> findAllProduit() {
		try {
			return em.createQuery("select r from Produit r").getResultList();
		} catch (Exception e) {
			System.out.println("findAllProduit --> e.getMessage() = " + e.getMessage());
			return null;
		}
	}
}
