package com.uvt.service;
import java.util.List;
import javax.persistence.EntityManager;
//import javax.persistence.PersistenceContext;
import com.uvt.util.JPAutil;
import com.uvt.entities.mbh_values;
public class mbh_valuesImpl extends AbstractFacade<mbh_values> implements mbh_valuesInterface {
	private EntityManager em = JPAutil.getEntityManager("MobiPhone");
	@Override
	protected EntityManager getEntityManager() {return em;}
	@SuppressWarnings("unchecked")
	@Override
	public List<mbh_values> findAllmbh_values() {
		try {
			return em.createQuery("select r from mbh_values r").getResultList();
		} catch (Exception e) {
			System.out.println("findAllmbh_values --> e.getMessage() = " + e.getMessage());
			return null;
		}
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<mbh_values> findAllmbh_values_byListName(String listName) {
		try {
			return em.createQuery("select r from "
					+ "mbh_values r join r.list l where l.name=:name")
					.setParameter("name", listName)
					.getResultList();
		} catch (Exception e) {
			System.out.println("findAllmbh_values_byListName --> e.getMessage() = " + e.getMessage());
			return null;
		}
	}

}
