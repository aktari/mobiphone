package com.uvt.service;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import com.uvt.util.JPAutil;
import com.uvt.entities.Cle3G;
public class Cle3GImpl extends AbstractFacade<Cle3G> implements Cle3GInterface {
	private EntityManager em = JPAutil.getEntityManager("MobiPhone");
	@Override
	protected EntityManager getEntityManager() {return em;}
	@Override
	public List<Cle3G> findAllCle3G() {
		try {
			return em.createQuery("select r from Cle3G r").getResultList();
		} catch (Exception e) {
			System.out.println("findAllCle3G --> e.getMessage() = " + e.getMessage());
			return null;
		}
	}
}
