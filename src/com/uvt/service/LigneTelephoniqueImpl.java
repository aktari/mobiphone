package com.uvt.service;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import com.uvt.util.JPAutil;
import com.uvt.entities.LigneTelephonique;
public class LigneTelephoniqueImpl extends AbstractFacade<LigneTelephonique> implements LigneTelephoniqueInterface {
	private EntityManager em = JPAutil.getEntityManager("MobiPhone");
	@Override
	protected EntityManager getEntityManager() {return em;}
	@Override
	public List<LigneTelephonique> findAllLigneTelephonique() {
		try {
			return em.createQuery("select r from LigneTelephonique r").getResultList();
		} catch (Exception e) {
			System.out.println("findAllLigneTelephonique --> e.getMessage() = " + e.getMessage());
			return null;
		}
	}
}
