package com.uvt.service;
import java.util.List;
import com.uvt.entities.Personne;
public interface PersonneInterface {
void create(Personne personne);
void edit(Personne personne);
void remove(Personne personne);
List<Personne> findAllPersonne();
}
