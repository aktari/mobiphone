package com.uvt.service;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import com.uvt.util.JPAutil;
import com.uvt.entities.TelMobileArticle;
public class TelMobileArticleImpl extends AbstractFacade<TelMobileArticle> implements TelMobileArticleInterface {
	private EntityManager em = JPAutil.getEntityManager("MobiPhone");
	@Override
	protected EntityManager getEntityManager() {return em;}
	@Override
	public List<TelMobileArticle> findAllTelMobileArticle() {
		try {
			return em.createQuery("select r from TelMobileArticle r").getResultList();
		} catch (Exception e) {
			System.out.println("findAllTelMobileArticle --> e.getMessage() = " + e.getMessage());
			return null;
		}
	}
}
