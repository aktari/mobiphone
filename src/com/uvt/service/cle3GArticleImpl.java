package com.uvt.service;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import com.uvt.util.JPAutil;
import com.uvt.entities.cle3GArticle;
public class cle3GArticleImpl extends AbstractFacade<cle3GArticle> implements cle3GArticleInterface {
	private EntityManager em = JPAutil.getEntityManager("MobiPhone");
	@Override
	protected EntityManager getEntityManager() {return em;}
	@Override
	public List<cle3GArticle> findAllcle3GArticle() {
		try {
			return em.createQuery("select r from cle3GArticle r").getResultList();
		} catch (Exception e) {
			System.out.println("findAllcle3GArticle --> e.getMessage() = " + e.getMessage());
			return null;
		}
	}
}
