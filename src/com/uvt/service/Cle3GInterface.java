package com.uvt.service;
import java.util.List;
import com.uvt.entities.Cle3G;
public interface Cle3GInterface {
void create(Cle3G cle3G);
void edit(Cle3G cle3G);
void remove(Cle3G cle3G);
List<Cle3G> findAllCle3G();
}
