package com.uvt.service;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import com.uvt.util.JPAutil;
import com.uvt.entities.Collaborateur;
public class CollaborateurImpl extends AbstractFacade<Collaborateur> implements CollaborateurInterface {
	private EntityManager em = JPAutil.getEntityManager("MobiPhone");
	@Override
	protected EntityManager getEntityManager() {return em;}
	@Override
	public List<Collaborateur> findAllCollaborateur() {
		try {
			return em.createQuery("select r from Collaborateur r").getResultList();
		} catch (Exception e) {
			System.out.println("findAllCollaborateur --> e.getMessage() = " + e.getMessage());
			return null;
		}
	}
}
