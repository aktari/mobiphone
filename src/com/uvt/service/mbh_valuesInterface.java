package com.uvt.service;
import java.util.List;

import com.uvt.entities.mbh_values;
public interface mbh_valuesInterface {
void create(mbh_values mbh_values);
void edit(mbh_values mbh_values);
void remove(mbh_values mbh_values);
List<mbh_values> findAllmbh_values();
List<mbh_values> findAllmbh_values_byListName(String listName);
}
