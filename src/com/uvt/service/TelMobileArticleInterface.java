package com.uvt.service;
import java.util.List;
import com.uvt.entities.TelMobileArticle;
public interface TelMobileArticleInterface {
void create(TelMobileArticle telMobileArticle);
void edit(TelMobileArticle telMobileArticle);
void remove(TelMobileArticle telMobileArticle);
List<TelMobileArticle> findAllTelMobileArticle();
}
