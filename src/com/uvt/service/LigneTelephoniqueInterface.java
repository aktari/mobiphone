package com.uvt.service;
import java.util.List;
import com.uvt.entities.LigneTelephonique;
public interface LigneTelephoniqueInterface {
void create(LigneTelephonique ligneTelephonique);
void edit(LigneTelephonique ligneTelephonique);
void remove(LigneTelephonique ligneTelephonique);
List<LigneTelephonique> findAllLigneTelephonique();
}
