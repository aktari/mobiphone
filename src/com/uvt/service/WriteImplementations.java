package com.uvt.service;

import java.util.*;

import javax.persistence.EntityManager;

import com.uvt.entities.TelephoneMobile;
import com.uvt.util.JPAutil;

import java.nio.file.*;
import  java.nio.charset.Charset;
import static java.util.stream.Stream.of;
import static java.util.stream.Collectors.toCollection;

public class WriteImplementations {

    public static void main(String[] args) {
    	Path path = FileSystems.getDefault().getPath(".").toAbsolutePath();
    	System.out.println(path.toString());
    	/*
    	ArrayList<String> lstEntite = of("LigneTelephonique",  
    		"CarteTelephonique", "Cle3G", "Client", "ClientMP", 
    		"Collaborateur", "Personne", "Produit", 
    		"Utilisateur")
    			.collect(toCollection(ArrayList::new));
    	ArrayList<String> lstEntite = of("CarteTelephoniqueArticle", 
    			"cle3GArticle", "TelMobileArticle")
    			.collect(toCollection(ArrayList::new));*/
    	ArrayList<String> lstEntite = of("mbh_lists", "mbh_values")
    			.collect(toCollection(ArrayList::new));
    	List<String> lines = new ArrayList<String>();
    	for (String entite : lstEntite) {
	    	//String entite = "LigneTelephonique";
	    	String nomEntite = "";
	    	for (int i=0; i < entite.length(); i++) {
	    		nomEntite+=(i==0)?Character.toLowerCase(entite.charAt(i)):entite.charAt(i);
	    	}
	    	System.out.println(entite);
	    	lines.clear();
			lines.addAll(Arrays.asList("package com.uvt.service;", 
					"import java.util.List;",
					"import javax.persistence.EntityManager;",
					"import javax.persistence.PersistenceContext;",
					"import com.uvt.util.JPAutil;",
					"import com.uvt.entities."+entite+";",
					"public class "+entite+"Impl extends AbstractFacade<"+entite+"> implements "+entite+"Interface {",
					"	private EntityManager em = JPAutil.getEntityManager(\"MobiPhone\");",
					"	@Override",
					"	protected EntityManager getEntityManager() {return em;}",
					"	@Override",
					"	public List<"+entite+"> findAll"+entite+"() {",
					"		try {",
					"			return em.createQuery(\"select r from "+entite+" r\").getResultList();",
					"		} catch (Exception e) {",
					"			System.out.println(\"findAll"+entite+" --> e.getMessage() = \" + e.getMessage());",
					"			return null;",
					"		}",
					"	}",
					"}"
					));
			
			Path file = Paths.get(entite+"Impl.java");
			try {
				Files.write(file, lines, Charset.forName("UTF-8"));
				System.out.println("write done ...");
			} catch(java.io.IOException ex) {
				System.out.println("IOException error thrown");
			}
    	}
		//Files.write(file, lines, Charset.forName("UTF-8"), StandardOpenOption.APPEND);

    }

}
