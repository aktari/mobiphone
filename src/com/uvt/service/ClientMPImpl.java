package com.uvt.service;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import com.uvt.util.JPAutil;
import com.uvt.entities.ClientMP;
public class ClientMPImpl extends AbstractFacade<ClientMP> implements ClientMPInterface {
	private EntityManager em = JPAutil.getEntityManager("MobiPhone");
	@Override
	protected EntityManager getEntityManager() {return em;}
	@Override
	public List<ClientMP> findAllClientMP() {
		try {
			return em.createQuery("select r from ClientMP r").getResultList();
		} catch (Exception e) {
			System.out.println("findAllClientMP --> e.getMessage() = " + e.getMessage());
			return null;
		}
	}
}
