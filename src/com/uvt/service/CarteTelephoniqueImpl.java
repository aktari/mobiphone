package com.uvt.service;
import java.util.List;
import javax.persistence.EntityManager;
import com.uvt.util.JPAutil;
import com.uvt.entities.CarteTelephonique;
public class CarteTelephoniqueImpl extends AbstractFacade<CarteTelephonique> implements CarteTelephoniqueInterface {
	private EntityManager em = JPAutil.getEntityManager("MobiPhone");
	@Override
	protected EntityManager getEntityManager() {return em;}
	@SuppressWarnings("unchecked")
	@Override
	public List<CarteTelephonique> findAllCarteTelephonique() {
		try {
			return em.createQuery("select r from CarteTelephonique r").getResultList();
		} catch (Exception e) {
			System.out.println("findAllCarteTelephonique --> e.getMessage() = " + e.getMessage());
			return null;
		}
	}
}
