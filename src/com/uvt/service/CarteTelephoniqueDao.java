package com.uvt.service;

import javax.persistence.EntityManager;
import javax.persistence.EntityTransaction;

import com.uvt.entities.CarteTelephonique;
import com.uvt.util.JPAutil;

public class CarteTelephoniqueDao {

	private EntityManager entityManager=JPAutil.getEntityManager("MobiPhone");

	public   void ajouter(CarteTelephonique c)
	{
	 	EntityTransaction tx = entityManager.getTransaction();
	 	tx.begin();
	 	entityManager.persist(c);
	 	tx.commit();
	}

	
}
