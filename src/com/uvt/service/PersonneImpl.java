package com.uvt.service;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import com.uvt.util.JPAutil;
import com.uvt.entities.Personne;
public class PersonneImpl extends AbstractFacade<Personne> implements PersonneInterface {
	private EntityManager em = JPAutil.getEntityManager("MobiPhone");
	@Override
	protected EntityManager getEntityManager() {return em;}
	@Override
	public List<Personne> findAllPersonne() {
		try {
			return em.createQuery("select r from Personne r").getResultList();
		} catch (Exception e) {
			System.out.println("findAllPersonne --> e.getMessage() = " + e.getMessage());
			return null;
		}
	}
}
