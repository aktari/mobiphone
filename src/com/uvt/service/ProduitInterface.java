package com.uvt.service;
import java.util.List;
import com.uvt.entities.Produit;
public interface ProduitInterface {
void create(Produit produit);
void edit(Produit produit);
void remove(Produit produit);
List<Produit> findAllProduit();
}
