package com.uvt.service;
import java.util.List;
import com.uvt.entities.ClientMP;
public interface ClientMPInterface {
void create(ClientMP clientMP);
void edit(ClientMP clientMP);
void remove(ClientMP clientMP);
List<ClientMP> findAllClientMP();
}
