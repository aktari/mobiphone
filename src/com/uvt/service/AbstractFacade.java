package com.uvt.service;

import javax.persistence.EntityManager;
import javax.persistence.EntityTransaction;

public abstract class AbstractFacade<T> {
    //private Class<T> entityClass;

    //public AbstractFacade(Class<T> entityClass) {
    //    this.entityClass = entityClass;
    //}

    protected abstract EntityManager getEntityManager();

    public void create(T entity) {
    	EntityTransaction tx = getEntityManager().getTransaction();
	 	tx.begin();
	 	getEntityManager().persist(entity);
	 	tx.commit();
    }

    public void edit(T entity) {
    	EntityTransaction tx = getEntityManager().getTransaction();
	 	tx.begin();
        getEntityManager().merge(entity);
        tx.commit();
    }

    public void remove(T entity) {
    	EntityTransaction tx = getEntityManager().getTransaction();
	 	tx.begin();
        getEntityManager().remove(getEntityManager().merge(entity));
        tx.commit();
    }

}
