package com.uvt.service;
import java.util.List;
import com.uvt.entities.cle3GArticle;
public interface cle3GArticleInterface {
void create(cle3GArticle cle3GArticle);
void edit(cle3GArticle cle3GArticle);
void remove(cle3GArticle cle3GArticle);
List<cle3GArticle> findAllcle3GArticle();
}
