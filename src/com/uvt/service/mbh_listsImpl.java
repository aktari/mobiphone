package com.uvt.service;
import java.util.List;
import javax.persistence.EntityManager;
import com.uvt.util.JPAutil;
import com.uvt.entities.mbh_lists;
public class mbh_listsImpl extends AbstractFacade<mbh_lists> implements mbh_listsInterface {
	private EntityManager em = JPAutil.getEntityManager("MobiPhone");
	@Override
	protected EntityManager getEntityManager() {return em;}
	@Override
	public List<mbh_lists> findAllmbh_lists() {
		try {
			@SuppressWarnings("unchecked")
			List<mbh_lists> lst = em.createQuery("select r from mbh_lists r").getResultList();
			return lst;
		} catch (Exception e) {
			System.out.println("findAllmbh_lists --> e.getMessage() = " + e.getMessage());
			return null;
		}
	}
	@Override
	public mbh_lists findMbh_lists_byName(String name) {
		try {
			return (mbh_lists) em.createQuery("select r from mbh_lists r where name ="+name).getResultList();
		} catch (Exception e) {
			System.out.println("findMbh_lists_byName --> e.getMessage() = " + e.getMessage());
			return null;
		}
	}
}
