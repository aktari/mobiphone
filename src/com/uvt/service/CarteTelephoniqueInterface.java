package com.uvt.service;
import java.util.List;
import com.uvt.entities.CarteTelephonique;
public interface CarteTelephoniqueInterface {
void create(CarteTelephonique carteTelephonique);
void edit(CarteTelephonique carteTelephonique);
void remove(CarteTelephonique carteTelephonique);
List<CarteTelephonique> findAllCarteTelephonique();
}
