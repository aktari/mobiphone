package com.uvt.service;
import java.util.List;
import com.uvt.entities.CarteTelephoniqueArticle;
public interface CarteTelephoniqueArticleInterface {
void create(CarteTelephoniqueArticle carteTelephoniqueArticle);
void edit(CarteTelephoniqueArticle carteTelephoniqueArticle);
void remove(CarteTelephoniqueArticle carteTelephoniqueArticle);
List<CarteTelephoniqueArticle> findAllCarteTelephoniqueArticle();
}
