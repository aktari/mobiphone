package com.uvt.service;

import java.util.List;

import com.uvt.entities.TelephoneMobile;

public interface TelephoneMobileInterface {

	void create(TelephoneMobile telephoneMobile);
    void edit(TelephoneMobile telephoneMobile);
    void remove(TelephoneMobile telephoneMobile);
    
    List<TelephoneMobile> findAllTelephoneMobile();
	
}
