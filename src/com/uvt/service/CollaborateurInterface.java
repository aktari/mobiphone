package com.uvt.service;
import java.util.List;
import com.uvt.entities.Collaborateur;
public interface CollaborateurInterface {
void create(Collaborateur collaborateur);
void edit(Collaborateur collaborateur);
void remove(Collaborateur collaborateur);
List<Collaborateur> findAllCollaborateur();
}
