package com.uvt.service;
import java.util.List;
import com.uvt.entities.mbh_lists;
public interface mbh_listsInterface {
void create(mbh_lists mbh_lists);
void edit(mbh_lists mbh_lists);
void remove(mbh_lists mbh_lists);
List<mbh_lists> findAllmbh_lists();
mbh_lists findMbh_lists_byName(String name);
}
